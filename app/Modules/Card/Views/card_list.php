<div class="row">
    <div class="col-md-12 col-md-12">
        <div class="card ">
            <div class="card-header py-2">
                            <div class="d-flex justify-content-between align-items-center">
                                <div>
                                    <h6 class="fs-17 font-weight-600 mb-0"><?php echo lan('card_list')?></h6>
                                </div>
                                <div class="text-right">
                                 <?php if($permission->method('add_card','create')->access()){?>    
                                   <a href="<?php echo base_url('card/add_card')?>" class="btn btn-success btn-sm mr-1"><i class="fas fa-plus mr-1"></i><?php echo lan('add_card')?></a>
                               <?php }?>
                                 
                                </div>
                            </div>
                        </div>
            <div class="card-body">

                <div class="table-responsive">
                    <table class="table display table-bordered table-striped table-hover custom-table" width="100%" id="CardList">
                        <thead>
                            <tr>
                            <th><?php echo lan('sl_no') ?></th>
                            <th width="200px;"><?php echo lan('card_name') ?></th>
                            <th><?php echo lan('card_type'); ?></th>
                            <th><?php echo lan('card_number') ?></th>
                            <th><?php echo lan('expiry_date'); ?></th>
                            <th><?php echo lan('balance') ?></th>
                            <th width="30px;"><?php echo lan('action') ?></th>
                              
                            </tr>
                        </thead>
                        <tbody>
                          
                          
                        </tbody>
                           <tfoot>
                                <tr>
                                    <th colspan="5" class="text-right"><?php echo lan('total') ?>:</th>
                                    <th class="text-right"></th>
                                   <th></th>
                                </tr>
                                            
                           </tfoot>
                    </table>
                    
                </div>
            </div> 
        </div>
    </div>
</div>
<script>
$(document).ready(function () {
        var t = $('[name="csrf_test_name"]').val(),
            e = $("#currency").val(),
            a = $("#base_url").val();
        $("#CardList").DataTable({
            responsive: !0,
            dom: "<'row'<'col-md-6'B><'col-md-6'f>>rt<'bottom'ip><'clear'>",
            aaSorting: [[1, "asc"]],
            columnDefs: [{ bSortable: !1, aTargets: [0, 2, 3, 4, 5, 6] }],
            processing: !0,
            serverSide: !0,
            lengthMenu: [
                [20, 35, 50, 100, 250, 500, -1],
                [20, 35, 50, 100, 250, 500, "All"],
            ],
            buttons: [
                { extend: "copyHtml5", text: '<i class="far fa-copy"></i>', titleAttr: "Copy", className: "btn-light" },
                { extend: "excelHtml5", text: '<i class="far fa-file-excel"></i>', titleAttr: "Excel", className: "btn-light" },
                { extend: "csvHtml5", text: '<i class="far fa-file-alt"></i>', titleAttr: "CSV", className: "btn-light" },
                { extend: "pdfHtml5", text: '<i class="far fa-file-pdf"></i>', titleAttr: "PDF", className: "btn-light" },
            ],
            serverMethod: "post",
            ajax: {
                url: a + "/card/card_checkdata",
                data: function (e) {
                    e.app_csrf = t;
                },
            },
            columns: [
                { data: "sl" },
                { data: "card_name" },
                { data: "card_type" },
                { data: "card_number" },
                { data: "expire_date" },
                { data: "balance", class: "balance text-right", render: $.fn.dataTable.render.number(",", ".", 2, e) },
                { data: "button" },
            ],
            footerCallback: function (t, a, l, n, o) {
                this.api()
                    .columns(".balance", { page: "current" })
                    .every(function () {
                        var t = this.data().reduce(function (t, e) {
                            return (parseFloat(t) || 0) + (parseFloat(e) || 0);
                        }, 0);
                        $(this.footer()).html(e + " " + t.toFixed(2, 2));
                    });
            },
        });
    })
</script>
