  <div class="row">
      <div class="col-md-12 col-lg-12">
          <div class="card">
              <div class="card-header py-2">
                  <div class="d-flex justify-content-between align-items-center">
                      <div>
                          <h6 class="fs-17 font-weight-600 mb-0"><?php echo lan('add_card') ?></h6>
                      </div>
                      <div class="text-right">
                          <?php if ($permission->method('card_list', 'read')->access()) { ?>
                              <a href="<?php echo base_url('card/card_list') ?>" class="btn btn-success btn-sm mr-1"><i class="fas fa-align-justify mr-1"></i><?php echo lan('card_list') ?></a>
                          <?php } ?>

                      </div>
                  </div>
              </div>
              <div class="card-body">


                  <?php echo form_open_multipart("card/add_card/" . $card->card_id) ?>

                  <input type="hidden" name="card_id" id="card_id" value="<?php echo $card->card_id ?>">
                  <div class="form-group row">
                      <label for="card_name" class="col-md-2 text-right col-form-label"><?php echo lan('card_name') ?> <i class="text-danger"> * </i>:</label>
                      <div class="col-md-4">
                          <div class="">

                              <input type="text" name="card_name" class="form-control" id="card_name" placeholder="<?php echo lan('card_name') ?>" value="<?php echo $card->card_name ?>">
                              <input type="hidden" name="old_name" value="<?php echo $card->card_name ?>">

                          </div>

                      </div>
                      <label for="ac_name" class="col-md-2 text-right col-form-label"><?php echo lan('card_type') ?> <i class="text-danger"> * </i>:</label>
                      <div class="col-md-4">
                          <div class="">
                          <select name="card_type" id="card_type" class="form-control select2" tabindex="4">
                            <option value="Visa" selected="selected">Visa</option>
                            <option value="Master">Master</option>
                            
                        </select>

                          </div>

                      </div>
                  </div>
                  <div class="form-group row">
                      <label for="ac_number" class="col-md-2 text-right col-form-label"><?php echo lan('card_number') ?> <i class="text-danger"> * </i>:</label>
                      <div class="col-md-4">
                          <div class="">

                              <input type="text" class="form-control input-mask-trigger valid_number" name="card_number" id="card_number" data-inputmask="'alias': 'email'" im-insert="true" placeholder="<?php echo lan('card_number') ?>" value="<?php echo $card->card_number ?>">

                          </div>

                      </div>
                      <label for="branch" class="col-md-2 text-right col-form-label"><?php echo lan('expiry_date') ?>:</label>
                      <div class="col-md-4">
                          <div class="">
                             <input type="text" name="expire_date" class="form-control datepicker" id="expire_date" placeholder="<?php echo lan('expiry_date')?>" value="<?php echo $card->expire_date ?>">
                              
                          </div>

                      </div>
                  </div>
                  <div class="form-group row">
                      <!-- <label for="signature_pic" class="col-md-2 text-right col-form-label"><?php // echo lan('signature_pic') ?>:</label>
                      <div class="col-md-4">
                          <div class="">

                              <input class="form-control input-mask-trigger text-left" id="signature_pic" type="file" name="image" placeholder="<?php // echo lan('signature_pic') ?>" data-inputmask="'alias': 'decimal', 'groupSeparator': '', 'autoGroup': true" im-insert="true">
                              <input type="hidden" name="old_image" value="<?php // echo $card->signature_pic ?>">
                          </div>

                      </div> -->

                      <label for="status" class="col-md-2 text-right col-form-label"><?php echo lan('status') ?> <i class="text-danger"> * </i>:</label>
                      <div class="col-md-4">
                          <div class="">

                              <label class="radio-inline my-2">
                                  <?php echo form_radio('status', '1', (($card->status == 1 || $card->status == null) ? true : false), 'id="status"'); ?>Active
                              </label>
                              <label class="radio-inline my-2">
                                  <?php echo form_radio('status', '0', (($card->status == "0") ? true : false), 'id="status"'); ?>Inactive
                              </label>

                          </div>

                      </div>
                  </div>
                 



                  <div class="form-group row">
                      <div class="col-md-6 text-right">
                      </div>
                      <div class="col-md-6 text-right">
                          <div class="">

                              <button type="submit" class="btn btn-success">
                                  <?php echo (empty($card->card_id) ? lan('save') : lan('update')) ?></button>

                          </div>

                      </div>
                  </div>


                  <?php echo form_close(); ?>
              </div>
          </div>
      </div>
  </div>