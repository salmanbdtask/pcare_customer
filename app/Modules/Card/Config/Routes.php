<?php

if(!isset($routes))
{ 
    $routes = \Config\Services::routes(true);
}

$routes->group('card', ['namespace' => 'App\Modules\Card\Controllers'], function($subroutes){

	/*** Route for card ***/
	$subroutes->add('add_card', 'Card::bdtask_0001_card_form');
	$subroutes->add('add_card/(:num)', 'Card::bdtask_0001_card_form/$1');
	$subroutes->add('edit_card/(:num)', 'Card::bdtask_0001_card_form/$1');
	$subroutes->add('delete_card/(:num)', 'Card::delete_card/$1');
	$subroutes->add('card_list', 'Card::index');
	$subroutes->add('card_checkdata', 'Card::bdtask_CheckcardList');
	
	
});

