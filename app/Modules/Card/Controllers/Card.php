<?php namespace App\Modules\Card\Controllers;
class Card extends BaseController
{
    #------------------------------------    
    # Author: Bdtask Ltd
    # Author link: https://www.bdtask.com/
    # Dynamic style php file
    # Developed by :Isahaq
    #------------------------------------    

    public function index()
	{
         if (!$this->session->get('isLogIn')){
        return redirect()->route('login');
    }

	    $data['title']      = 'card List';
        $data['module']     = "card";
        $data['page']       = "card_list"; 
		return $this->template->layout($data);

	}

     public function bdtask_CheckcardList()
     {
        $postData = $this->request->getVar();
        $data     = $this->cardModel->getcardList($postData);
        echo json_encode($data);
    } 

        public function bdtask_0001_card_form($id = null)
        {
        helper(['form','url']);
        $id = (!empty($id)?$id:$this->request->getVar('card_id', FILTER_SANITIZE_STRING));
        $this->validation =  \Config\Services::validation();
        

        
        $data = [];
           $data['card'] = (object)$userLevelData = array(
            'card_id'          => $this->request->getVar('card_id', FILTER_SANITIZE_STRING),
            'card_name'        => $this->request->getVar('card_name', FILTER_SANITIZE_STRING),
            'card_type'          => $this->request->getVar('card_type', FILTER_SANITIZE_STRING),
            'card_number'        => $this->request->getVar('card_number', FILTER_SANITIZE_STRING),
            'expire_date'           => $this->request->getVar('expire_date', FILTER_SANITIZE_STRING),
            'status'           => $this->request->getVar('status', FILTER_SANITIZE_STRING)
        );

        if ($this->request->getMethod() == 'post') {
            if(empty($id)){
            $rules = [
                'card_name'   => ['label' => lan('card_name'),'rules' => 'required|min_length[3]|max_length[150]|is_unique[card_information.card_name]'],
                'card_number'   => ['label' => lan('card_number'),'rules' => 'required|min_length[6]|max_length[20]|is_unique[card_information.card_number]'],
                'card_type'     => ['label' => lan('card_type'),'rules'   => 'required'], 
            ];
        }else{
             $rules = [
                'card_name' => ['label' => lan('card_name'),'rules' => 'required|min_length[3]|max_length[20]'],
                'card_number' => ['label' => lan('card_number'),'rules' => 'required|min_length[6]|max_length[20]'],
                'card_type'  => ['label' => lan('card_type'),'rules'   => 'required'], 
                 
            ];
        }

            if (! $this->validate($rules)) {
                $this->session->setFlashdata(array('exception'=>$this->validator->listErrors()));
                  return  redirect()->to(base_url('card/add_card'));
            }else{
               if(empty($id)){
                $this->cardModel->save_card($userLevelData);
                $this->session->setFlashdata('message', lan('save_successfully'));
                return  redirect()->to(base_url('/card/card_list/'));
               
            }else{
             $this->cardModel->update_card($userLevelData);
             $this->session->setFlashdata('message', lan('successfully_updated'));
             
              return  redirect()->to(base_url('/card/card_list/'));
               
            }

            }
        }

        $data['module']      = "card";
        if(!empty($id)){
        $data['card']        = $this->cardModel->singledata($id); }
        $data['title']       = 'card';
        $data['page']        = "card_form"; 
        return $this->template->layout($data);
    }

    public function delete_card($id = null)
    { 
        if ($this->cardModel->delete_card($id)) {
            $this->session->setFlashdata('message', lan('successfully_deleted'));
        } else {
            $this->session->setFlashdata('exception', lan('please_try_again'));
        }

        return redirect()->route('card/card_list');
    }

}
