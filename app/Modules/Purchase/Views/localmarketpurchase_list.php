<div class="row">
    <div class="col-md-12 col-md-12">
        <div class="card ">
            <div class="card-header py-2">
                            <div class="d-flex justify-content-between align-items-center">
                                <div>
                                    <h6 class="fs-17 font-weight-600 mb-0"><?php echo lan('localmarket_purchase_list')?></h6>
                                </div>
                                <div class="text-right">
                                  <?php if($permission->method('add_purchase','create')->access()){?>
                                   <a href="<?php echo base_url('purchase/localmarket_purchase')?>" class="btn btn-success btn-sm mr-1"><i class="fas fa-plus mr-1"></i><?php echo lan('localmarket_purchase')?></a>
                                 <?php }?>
                                 
                                </div>
                            </div>
                        </div>
            <div class="card-body">
             <div class="row">
                  <div class="col-sm-12">
                          <form action="" class="form-inline" method="post" accept-charset="utf-8">
                                            <label class="sr-only" for="from_date"><?php echo lan('start_date') ?></label>
                                            <div class="input-group mb-2 mr-sm-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text"><?php echo lan('start_date') ?></div>
                                                </div>
                                                <input type="text" class="form-control datepicker psdate" name="from_date" id="from_date" placeholder="<?php echo lan('start_date') ?>" value="">
                                            </div>

                                            <label class="sr-only" for="to_date"><?php echo lan('end_date') ?></label>
                                            <div class="input-group mb-2 mr-sm-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text"><?php echo lan('end_date') ?></div>
                                                </div>
                                                <input type="text" class="form-control datepicker pedate" id="to_date" name="to_date" placeholder="<?php echo lan('end_date') ?>">
                                            </div>
                                        
                                            <button type="button" id="btn-filter-pur" class="btn btn-success mb-2"><?php echo lan('find') ?></button>
                                        </form>
                </div>
               
            </div>
                <div class="table-responsive">
                     <table class="table table-striped table-bordered custom-table" cellspacing="0" width="100%" id="LocPurList"> 
                <thead>
                  <tr>
                    <th><?php echo lan('sl_no') ?></th>
                    <th><?php echo lan('invoice_no') ?></th>
                    <th><?php echo lan('purchase_id') ?></th>
                    <th><?php echo lan('manufacturer_name') ?></th>
                    <th><?php echo lan('date') ?></th>
                    <th><?php echo lan('total_amount') ?></th>
                    <th><?php echo lan('action') ?></th>
                  </tr>
                </thead>
                <tbody>
            
                </tbody>
                <tfoot>
                    <th colspan="5" class="text-right">Total:</th>
                
                  <th></th>  
                  <th></th> 
                                </tfoot>
                        </table>
                    
                </div>
            </div> 
        </div>
    </div>
</div>

<script>
$(document).ready(function () {
        var t = $('[name="csrf_test_name"]').val(),
            e = $("#currency").val(),
            a = $("#base_url").val(),
            l = $("#LocPurList").DataTable({
                responsive: !0,
                dom: "<'row'<'col-md-6'B><'col-md-6'f>>rt<'bottom'ip><'clear'>",
                aaSorting: [[4, "desc"]],
                columnDefs: [{ bSortable: !1, aTargets: [0, 1, 2, 3, 5, 6] }],
                processing: !0,
                serverSide: !0,
                lengthMenu: [
                    [15, 25, 50, 100, 250, 500, -1],
                    [15, 25, 50, 100, 250, 500, "All"],
                ],
                buttons: [
                    { extend: "copyHtml5", text: '<i class="far fa-copy"></i>', titleAttr: "Copy", className: "btn-light" },
                    { extend: "excelHtml5", text: '<i class="far fa-file-excel"></i>', titleAttr: "Excel", className: "btn-light" },
                    { extend: "csvHtml5", text: '<i class="far fa-file-alt"></i>', titleAttr: "CSV", className: "btn-light" },
                    { extend: "pdfHtml5", text: '<i class="far fa-file-pdf"></i>', titleAttr: "PDF", className: "btn-light" },
                ],
                serverMethod: "post",
                ajax: {
                    url: a + "/purchase/purchase_list_check_local",
                    data: function (e) {
                        (e.fromdate = $("#from_date").val()), (e.todate = $("#to_date").val()), (e.app_csrf = t);
                    },
                },
                columns: [
                    { data: "sl" },
                    { data: "chalan_no" },
                    { data: "purchase_id" },
                    { data: "manufacturer_name" },
                    { data: "purchase_date" },
                    { data: "total_amount", class: "total_sale text-right", render: $.fn.dataTable.render.number(",", ".", 2, e) },
                    { data: "button" },
                ],
                footerCallback: function (t, a, l, n, o) {
                    this.api()
                        .columns(".total_sale", { page: "current" })
                        .every(function () {
                            var t = this.data().reduce(function (t, e) {
                                return (parseFloat(t) || 0) + (parseFloat(e) || 0);
                            }, 0);
                            $(this.footer()).html(e + " " + t.toLocaleString(void 0, { minimumFractionDigits: 2, maximumFractionDigits: 2 }));
                        });
                },
            });
        $("#btn-filter-pur").on("click", function (t) {
            l.ajax.reload();
        });
    })
</script>

