<?php namespace App\Modules\Medicine\Controllers;
class Generic extends BaseController
{
    #------------------------------------    
    # Author: Bdtask Ltd
    # Author link: https://www.bdtask.com/
    # Dynamic style php file
    # Developed by :Isahaq
    #------------------------------------    

    public function index()
	{
         if (!$this->session->get('isLogIn')){
        return redirect()->route('login');
    }

	    $data['title']         = 'Generic List';
        $data['module']        = "Medicine";
        $data['generic_list'] = $this->genericModel->findAll();
        $data['page']          = "generic_list"; 
		return $this->template->layout($data);

	}


        public function bdtask_0001_generic_form($id = null)
        {
        if (!$this->session->get('isLogIn')){
        return redirect()->route('login');}
        $id = (!empty($id)?$id:$this->request->getVar('generic_id'));
        $data = [];
           $data['generic'] = (object)$userLevelData = array(
            'generic_id'    => ($this->request->getVar('generic_id')?$this->request->getVar('generic_id'):null),
            'generic_name'  => $this->request->getVar('generic_name', FILTER_SANITIZE_STRING),
            'status'         => $this->request->getVar('status', FILTER_SANITIZE_STRING),
        );

        if ($this->request->getMethod() == 'post') {
          
            $rules = [
                'generic_name'   => ['label' => lan('generic_name'),'rules' => 'required'],

                'status'          => ['label' => lan('status'),'rules'        => 'required|min_length[1]|max_length[3]'], 
            ];
      

            if (! $this->validate($rules)) {
                $data['validation'] = $this->validator;
            }else{
               if(empty($id)){
                $this->genericModel->save_generic($userLevelData);
                $this->session->setFlashdata('message', lan('save_successfully'));
                return  redirect()->to(base_url('/medicine/generic_list/'));
               
            }else{
             $this->genericModel->update_generic($userLevelData);
             $this->session->setFlashdata('message', lan('successfully_updated'));
             
              return  redirect()->to(base_url('/medicine/generic_list/'));
               
            }

            }
        }

        $data['module']      = "Medicine";
        if(!empty($id)){
        $data['generic']    = $this->genericModel->singledata($id); }
        $data['title']       = 'generic';
        $data['page']        = "generic_form"; 
        return $this->template->layout($data);
    }

    public function delete_generic($id = null)
    { 
        if ($this->genericModel->delete_generic($id)) {
            $this->session->setFlashdata('message', lan('successfully_deleted'));
        } else {
            $this->session->setFlashdata('exception', lan('please_try_again'));
        }

        return redirect()->route('medicine/generic_list');
    }

}
