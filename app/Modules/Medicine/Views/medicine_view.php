  <div class="row">
      <div class="col-md-12 col-lg-12">
          <div class="card">
              <div class="card-header py-2">
                  <div class="d-flex justify-content-between align-items-center">
                      <div>
                          <h6 class="fs-17 font-weight-600 mb-0"><?php echo lan('medicine_details') ?></h6>
                      </div>
                      <div class="text-right">
                          <?php if ($permission->method('medicine_list', 'read')->access()) { ?>
                              <a href="<?php echo base_url('medicine/medicine_list') ?>" class="btn btn-success btn-sm mr-1"><i class="fas fa-align-justify mr-1"></i><?php echo lan('medicine_list') ?></a>
                              
                          <?php } ?>

                      </div>
                  </div>
              </div>
              <div class="card-body">

                  <div class="form-group row">
                      
                      <label for="medicine_name" class="col-md-2 text-right col-form-label"><?php echo lan('medicine_name') ?> :</label>
                      <div class="col-md-4">
                          <div class="">
                              <label for="medicine_name" class=" text-right col-form-label"><?php echo $medicine->product_name ?></label>
                          </div>
                      </div>
                  </div>
                  <div class="form-group row">
                      <label for="strength" class="col-md-2 text-right col-form-label"><?php echo lan('strength') ?> <i class="text-danger"> </i>:</label>
                      <div class="col-md-4">
                          <div class="">
                              <label for="medicine_name" class="text-right col-form-label"><?php echo $medicine->strength ?></label>
                          </div>
                      </div>
                      <label for="generic_name" class="col-md-2 text-right col-form-label"><?php echo lan('generic_name') ?>:</label>
                      <div class="col-md-4">
                          <div class="">
                          <?php
                                foreach ($generic_list as $generic) {
                                    if ($generic->generic_name == $medicine->generic_name) { ?>
                                      <label for="email_address" class="text-right col-form-label"><?php echo $generic->generic_name; ?></label>
                              <?php  }
                                } ?>
                              
                          </div>
                      </div>
                  </div>

                  <div class="form-group row">
                      <label for="box_size" class="col-md-2 text-right col-form-label"><?php echo lan('box_size') ?> :</label>
                      <div class="col-md-4">
                          <div class="">
                          <?php
                                foreach ($leaf as $leafs) {
                                    if ($leafs->total_number == $medicine->box_size) { ?>
                                      <label for="email_address" class="text-right col-form-label"><?php echo $leafs->leaf_type; ?></label>
                              <?php  }
                                } ?>
                             
                          </div>
                      </div>
                      <label for="unit" class="col-md-2 text-right col-form-label"><?php echo lan('unit') ?> :</label>
                      <div class="col-md-4">
                          <div class="">
                          <?php
                                foreach ($unit_list as $unit) {
                                    
                                    if ($unit->id ==  $medicine->unit) { ?>
                                      <label for="email_address" class="text-right col-form-label"><?php echo $unit->unit_name; ?></label>
                              <?php  }
                                } ?>
                              
                          </div>
                      </div>
                  </div>

                  <div class="form-group row">
                      <label for="product_location" class="col-md-2 text-right col-form-label"><?php echo lan('product_location') ?>:</label>
                      <div class="col-md-4">
                          <div class="">

                              <label for="medicine_name" class="text-right col-form-label"><?php echo $medicine->product_location ?></label>

                          </div>
                      </div>
                      <label for="product_details" class="col-md-2 text-right col-form-label"><?php echo lan('product_details') ?>:</label>
                      <div class="col-md-4">
                          <div class="">

                              <label for="medicine_name" class="text-right col-form-label"><?php echo $medicine->product_details ?></label>

                          </div>
                      </div>
                  </div>
                  <div class="form-group row">


                      <label for="category" class="col-md-2 text-right col-form-label"><?php echo lan('category') ?>:</label>
                      <div class="col-md-4">
                          <div class="">
                          <?php
                                foreach ($category_list as $cat) {
                                    
                                    if ($cat->category_id ==  $medicine->category_id) { ?>
                                      <label for="email_address" class="text-right col-form-label"><?php echo $cat->category_name; ?></label>
                              <?php  }
                                } ?>
                              

                          </div>
                      </div>

                      <label for="price" class="col-md-2 text-right col-form-label"><?php echo lan('price') ?>:</label>
                      <div class="col-md-4">
                          <div class="">

                              <label for="medicine_name" class="text-right col-form-label"><?php echo $medicine->b_price ?></label>
                          </div>
                      </div>
                  </div>
                  <div class="form-group row">

                      <label for="medicine_type" class="col-md-2 text-right col-form-label"><?php echo lan('medicine_type') ?>:</label>
                      <div class="col-md-4">
                          <div class="">
                          <?php
                                foreach ($type_list as $type) {
                                    
                                    if ($type->id ==  $medicine->product_type) { ?>
                                      <label for="email_address" class="text-right col-form-label"><?php echo $type->type_name; ?></label>
                              <?php  }
                                } ?>
                              
                          </div>

                      </div>

                      <label for="manufacturer_price" class="col-md-2 text-right col-form-label"><?php echo lan('manufacturer_price') ?> :</label>
                      <div class="col-md-4">
                          <div class="">

                              <label for="medicine_name" class="text-right col-form-label"><?php echo $medicine->m_b_price ?></label>

                          </div>

                      </div>
                  </div>
                  <div class="form-group row">
                      <label for="manufacturer_id" class="col-md-2 text-right col-form-label"><?php echo lan('manufacturer') ?> :</label>
                      <div class="col-md-4">
                          <div class="">
                          <?php
                                foreach ($manufacturer_list as $manufacturer) {
                                    
                                    if ($manufacturer->manufacturer_id ==  $medicine->manufacturer_id) { ?>
                                      <label for="email_address" class="text-right col-form-label"><?php echo $manufacturer->manufacturer_name; ?></label>
                              <?php  }
                                } ?>
                          </div>
                      </div>
                      <label for="status" class="col-md-2 text-right col-form-label"><?php echo lan('status') ?> :</label>
                      <div class="col-md-4">
                          <div class="">
                          <?php
                                    if ($medicine->status == 1) { ?>
                                      <label for="email_address" class="text-right col-form-label"><?php echo 'Active'; ?></label>
                                      <?php  }else{
                                 ?>
                                     <label for="email_address" class="text-right col-form-label"><?php echo 'Inactive'; ?></label>
                              <?php  }
                                 ?>

                             

                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>