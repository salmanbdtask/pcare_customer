<?php namespace App\Modules\Medicine\Models;

class GenericModel
{
	
	 public function __construct()
    {
        $this->db = db_connect();
        $this->session = \Config\Services::session();
         helper(['form','url']);
         $this->request = \Config\Services::request();
    }

    public function findAll()
    {
        $builder = $this->db->table('product_generic');
		$builder->select("*");
         $query   = $builder->get(); 
		return $query->getResult();

      
    }

    public function singledata($id){
        $builder = $this->db->table('product_generic')
                             ->where('generic_id', $id)
                             ->get()
                             ->getRow(); 
		return $builder;


    }

    public function save_generic($data=[]){
        $builder = $this->db->table('product_generic');
         return $add_manufacturer = $builder->insert($data);
    }

    public function update_generic($data=[]){
     $query = $this->db->table('product_generic');   
     $query->where('generic_id', $data['generic_id']);
     return $cus_up =  $query->update($data);  
    }

    public function delete_generic($id){
            $builder = $this->db->table('product_generic');
            $builder->where('generic_id', $id);
     return $builder->delete();
    }

   





  

}