  <div class="row">
      <div class="col-md-12 col-lg-12">
          <div class="card">
              <div class="card-header py-2">
                  <div class="d-flex justify-content-between align-items-center">
                      <div>
                          <h6 class="fs-17 font-weight-600 mb-0"><?php echo lan('add_customer_type') ?></h6>
                      </div>
                      <div class="text-right">
                          <?php if ($permission->method('customer_type_list', 'read')->access()) { ?>
                              <a href="<?php echo base_url('customer/customer_type_list') ?>" class="btn btn-success btn-sm mr-1"><i class="fas fa-align-justify mr-1"></i><?php echo lan('customer_type_list') ?></a>
                          <?php } ?>

                      </div>
                  </div>
              </div>
              <div class="card-body">


                  <?php echo form_open_multipart("customer/add_customer_type/" . $customer->customer_id) ?>
                  <?php echo form_hidden('customer_id', $customer->customer_id) ?>
                  <div class="form-group row">
                      <label for="customer_name" class="col-sm-3 col-form-label"><?php echo lan('customer_type'); ?> <i class="text-danger">*</i></label>
                      <div class="col-sm-3">
                          <input name="customer_type" class="form-control" type="text" placeholder="<?php echo lan('customer_type'); ?>" id="customer_type" value="<?php echo $customer->customer_type; ?>">
                      </div>
                  </div>

                  <div class="form-group row">
                      <label for="status" class="col-sm-3 col-form-label">status <i class="text-danger">*</i></label>
                      <div class="col-sm-3">
                          <label class="radio-inline my-2">
                              <?php echo form_radio('status', '1', (($customer->status == 1 || $customer->status == null) ? true : false), 'id="status"'); ?>Active
                          </label>
                          <label class="radio-inline my-2">
                              <?php echo form_radio('status', '0', (($customer->status == "0") ? true : false), 'id="status"'); ?>Inactive
                          </label>
                      </div>
                  </div>
                  <div class="form-group row">

                      <div class="col-md-6 text-right">
                          <div class="">

                              <button type="submit" class="btn btn-success">
                                  <?php echo (empty($customer->customer_id) ? lan('save') : lan('update')) ?></button>

                          </div>

                      </div>
                  </div>

                  <?php echo form_close(); ?>
              </div>
          </div>
      </div>
  </div>