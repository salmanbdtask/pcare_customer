  <div class="row">
      <div class="col-md-12 col-lg-12">
          <div class="card">
              <div class="card-header py-2">
                  <div class="d-flex justify-content-between align-items-center">
                      <div>
                          <h6 class="fs-17 font-weight-600 mb-0"><?php echo lan('customer_details') ?></h6>
                      </div>
                      <div class="text-right">
                          <?php if ($permission->method('customer_list', 'read')->access()) { ?>
                              <a href="<?php echo base_url('customer/customer_list') ?>" class="btn btn-success btn-sm mr-1"><i class="fas fa-align-justify mr-1"></i><?php echo lan('customer_list') ?></a>

                              <a href="<?php echo base_url('customer/paid_customer') ?>" class="btn btn-purple btn-sm mr-1"><i class="fas fa-align-justify mr-1"></i><?php echo lan('paid_customer') ?></a>
                              <a href="<?php echo base_url('customer/credit_customer') ?>" class="btn btn-info btn-sm"><i class="fas fa-align-justify mr-1"></i><?php echo lan('credit_customer') ?></a>
                          <?php } ?>

                      </div>
                  </div>
              </div>
              <div class="card-body">

                  <div class="form-group row">
                      <label for="customer_type" class="col-md-2 text-right col-form-label"><?php echo lan('customer_type') ?> :</label>
                      <div class="col-md-4">
                          <div class="">
                              <?php
                                foreach ($customer_type_list as $cst) {
                                    if ($cst->customer_id == $customer->customer_type) { ?>
                                      <label for="email_address" class="text-right col-form-label"><?php echo $cst->customer_type; ?></label>
                              <?php  }
                                } ?>
                          </div>
                      </div>
                      <label for="pharmacy_owner" class="col-md-2 text-right col-form-label"><?php echo lan('pharmacy_owner') ?>:</label>
                      <div class="col-md-4">
                          <div class="">

                              <label for="email_address" class="text-right col-form-label"><?php echo $customer->pharmacy_owner ?></label>

                          </div>
                      </div>
                  </div>
                  <div class="form-group row">
                      <label for="customer_name" class="col-md-2 text-right col-form-label"><?php echo lan('customer_name') ?> :</label>
                      <div class="col-md-4">
                          <div class="">

                              <label for="email_address" class="text-right col-form-label"><?php echo $customer->customer_name ?></label>


                          </div>

                      </div>
                      <label for="customer_mobile" class="col-md-2 text-right col-form-label"><?php echo lan('mobile_no') ?> :</label>
                      <div class="col-md-4">
                          <div class="">

                              <label for="email_address" class="text-right col-form-label"><?php echo $customer->customer_mobile ?></label>

                          </div>

                      </div>
                  </div>
                  <div class="form-group row">
                      <label for="customer_email" class="col-md-2 text-right col-form-label"><?php echo lan('email_address') ?>1:</label>
                      <div class="col-md-4">
                          <div class="">

                              <label for="email_address" class="text-right col-form-label"><?php echo $customer->customer_email ?></label>

                          </div>

                      </div>
                      <label for="email_address" class="col-md-2 text-right col-form-label"><?php echo lan('email_address') ?>2:</label>
                      <div class="col-md-4">
                          <div class="">
                              <label for="email_address" class="text-right col-form-label"><?php echo $customer->email_address ?></label>
                          </div>
                      </div>
                  </div>
                  <div class="form-group row">
                      <label for="phone" class="col-md-2 text-right col-form-label"><?php echo lan('phone') ?>:</label>
                      <div class="col-md-4">
                          <div class="">
                              <label for="email_address" class="text-right col-form-label"><?php echo $customer->phone ?></label>


                          </div>

                      </div>

                      <label for="contact" class="col-md-2 text-right col-form-label"><?php echo lan('contact') ?>:</label>
                      <div class="col-md-4">
                          <div class="">
                              <label for="email_address" class="text-right col-form-label"><?php echo $customer->contact ?></label>
                          </div>

                      </div>
                  </div>
                  <div class="form-group row">
                      <label for="address1" class="col-md-2 text-right col-form-label"><?php echo lan('address1') ?>:</label>
                      <div class="col-md-4">
                          <div class="">
                              <label for="email_address" class="text-right col-form-label"><?php echo $customer->customer_address ?></label>
                          </div>

                      </div>

                      <label for="address2" class="col-md-2 text-right col-form-label"><?php echo lan('address2') ?>:</label>
                      <div class="col-md-4">
                          <div class="">

                              <label for="email_address" class="text-right col-form-label"><?php echo $customer->address2 ?></label>

                          </div>

                      </div>
                  </div>
                  <div class="form-group row">
                      <label for="fax" class="col-md-2 text-right col-form-label"><?php echo lan('fax') ?>:</label>
                      <div class="col-md-4">
                          <div class="">

                              <label for="email_address" class="text-right col-form-label"><?php echo $customer->fax ?></label>

                          </div>

                      </div>
                      <label for="city" class="col-md-2 text-right col-form-label"><?php echo lan('city') ?>:</label>
                      <div class="col-md-4">
                          <div class="">

                              <label for="email_address" class="text-right col-form-label"><?php echo $customer->city ?></label>

                          </div>

                      </div>
                  </div>
                  <div class="form-group row">
                      <label for="state" class="col-md-2 text-right col-form-label"><?php echo lan('state') ?>:</label>
                      <div class="col-md-4">
                          <div class="">

                              <label for="email_address" class="text-right col-form-label"><?php echo $customer->state ?></label>

                          </div>

                      </div>
                      <label for="zip" class="col-md-2 text-right col-form-label"><?php echo lan('zip') ?>:</label>
                      <div class="col-md-4">
                          <div class="">

                              <label for="email_address" class="text-right col-form-label"><?php echo $customer->zip ?></label>

                          </div>

                      </div>
                  </div>
                  <div class="form-group row">
                      <label for="country" class="col-md-2 text-right col-form-label"><?php echo lan('country') ?>:</label>
                      <div class="col-md-4">
                          <div class="">

                              <label for="email_address" class="text-right col-form-label"><?php echo $customer->country ?></label>

                          </div>

                      </div>
                  </div>

              </div>
          </div>
      </div>
  </div>