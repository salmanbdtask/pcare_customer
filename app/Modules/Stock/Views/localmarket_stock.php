<div class="row">
    <div class="col-md-12 col-md-12">
        <div class="card ">
            <div class="card-header py-2">
                            <div class="d-flex justify-content-between align-items-center">
                                <div>
                                    <h6 class="fs-17 font-weight-600 mb-0"><?php echo lan('stock_report')?></h6>
                                </div>
                                <div class="text-right">
                                   <a href="<?php echo base_url('stock/stock_list_batchWise')?>" class="btn btn-success btn-sm mr-1"><i class="fas fa-align-justify mr-1"></i><?php echo lan('stock_report_batchwise')?></a>
                                 
                                </div>
                            </div>
                        </div>
            <div class="card-body">

                <div class="table-responsive">
                    <table class="table display table-bordered table-striped table-hover custom-table" width="100%" id="LocalMarketStock">
                        <thead>
                            <tr>
                      <th class="text-center"><?php echo lan('sl_no') ?></th>
                      <th class="text-center"><nobr><?php echo lan('medicine_name') ?></nobr></th>
                      <th class="text-center"><nobr><?php echo lan('manufacturer_name') ?></nobr></th>
                      <th class="text-center"><nobr><?php echo lan('sale_price') ?></nobr></th>
                      <th class="text-center"><nobr><?php echo lan('purchase_price') ?></nobr></th>
                      <th class="text-center"><nobr><?php echo lan('in_qty') ?></nobr></th>
                      <th class="text-center"><nobr><?php echo lan('out_qty') ?></nobr></th>
                      <th class="text-center"><nobr><?php echo lan('stock') ?></nobr></th>
                      <th class="text-center"><nobr><?php echo lan('stock').' box'; ?></nobr></th>
                      <th class="text-center"><nobr><?php echo lan('stock_sale_price')?></nobr></th>
                      <th class="text-center"><nobr><?php echo lan('stock_purchase_price')?></nobr></th>
                              
                            </tr>
                        </thead>
                        <tbody>
                          
                          
                        </tbody>
                           <tfoot>
                      <tr>
                <th colspan="7" class="text-right"><?php echo lan('total')?>:</th>
                <th id="stockqty"></th>
                  <th></th><th></th>  <th></th> 
            </tr>
                      
                    </tfoot> 
                    </table>
                    
                </div>
            </div> 
        </div>
    </div>
</div>
<script>
 $(document).ready(function () {
        var t = $('[name="csrf_test_name"]').val(),
            e = $("#currency").val(),
            a = $("#base_url").val();
        $("#LocalMarketStock").DataTable({
            responsive: !0,
            dom: "<'row'<'col-md-6'B><'col-md-6'f>>rt<'bottom'ip><'clear'>",
            aaSorting: [[1, "asc"]],
            columnDefs: [{ bSortable: !1, aTargets: [0, 2, 3, 4, 5, 6, 7, 8, 9, 10] }],
            processing: !0,
            serverSide: !0,
            lengthMenu: [
                [25, 50, 100, 250, 500, -1],
                [25, 50, 100, 250, 500, "All"],
            ],
            buttons: [
                { extend: "copyHtml5", text: '<i class="far fa-copy"></i>', titleAttr: "Copy", className: "btn-light" },
                { extend: "excelHtml5", text: '<i class="far fa-file-excel"></i>', titleAttr: "Excel", className: "btn-light" },
                { extend: "csvHtml5", text: '<i class="far fa-file-alt"></i>', titleAttr: "CSV", className: "btn-light" },
                { extend: "pdfHtml5", text: '<i class="far fa-file-pdf"></i>', titleAttr: "PDF", className: "btn-light" },
            ],
            serverMethod: "post",
            ajax: {
                url: a + "/stock/localmarket_stock_checkdata",
                data: function (e) {
                    e.app_csrf = t;
                },
            },
            columns: [
                { data: "sl" },
                { data: "product_name" },
                { data: "manufacturer_name" },
                { data: "sales_price", class: "text-right", render: $.fn.dataTable.render.number(",", ".", 2, e) },
                { data: "purchase_p", class: "text-right", render: $.fn.dataTable.render.number(",", ".", 2, e) },
                { data: "totalPurchaseQnty" },
                { data: "totalSalesQnty" },
                { data: "stok_quantity", class: "stock" },
                { data: "stock_box", render: $.fn.dataTable.render.number(",", ".", 2) },
                { data: "total_sale_price", class: "total_sale text-right", render: $.fn.dataTable.render.number(",", ".", 2, e) },
                { data: "purchase_total", class: "total_purchase text-right", render: $.fn.dataTable.render.number(",", ".", 2, e) },
            ],
            footerCallback: function (t, a, l, n, o) {
                var i = this.api();
                i.columns(".stock", { page: "current" }).every(function () {
                    var t = this.data().reduce(function (t, e) {
                        return (parseFloat(t) || 0) + (parseFloat(e) || 0);
                    }, 0);
                    $(this.footer()).html(t.toLocaleString());
                }),
                    i.columns(".total_sale", { page: "current" }).every(function () {
                        var t = this.data().reduce(function (t, e) {
                            return (parseFloat(t) || 0) + (parseFloat(e) || 0);
                        }, 0);
                        $(this.footer()).html(e + " " + t.toLocaleString(void 0, { minimumFractionDigits: 2, maximumFractionDigits: 2 }));
                    }),
                    i.columns(".total_purchase", { page: "current" }).every(function () {
                        var t = this.data().reduce(function (t, e) {
                            return (parseFloat(t) || 0) + (parseFloat(e) || 0);
                        }, 0);
                        $(this.footer()).html(e + " " + t.toLocaleString(void 0, { minimumFractionDigits: 2, maximumFractionDigits: 2 }));
                    });
            },
        });
    })
</script>
